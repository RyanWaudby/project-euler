array = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
first = 55

while array.last < 3500000
  second = array.last
  next_number = first + second
  first = second
  array.push(next_number) 
end

total = 0
array.each do |x|
  total += x if x % 2 == 0
end

puts total
